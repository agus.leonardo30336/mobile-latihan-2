package com.example.latihan2

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {
    lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_run!!.setOnClickListener {
            when (in_time.text.isNullOrBlank()) {
                true -> Toast.makeText(this, "Please insert the time", Toast.LENGTH_SHORT).show()
                false -> {
                    progressDialog = ProgressDialog.show(
                        this,
                        "ProgressDialog",
                        "Wait for " + in_time!!.text.toString() + " seconds"
                    )
                    runAsyncTask(Integer.parseInt(in_time.text.toString()))
                }
            }
        }

    }

    private fun runAsyncTask(time: Int) {
        doAsync {
            uiThread {
                tv_result.text = "Sleeping..."
            }
            Thread.sleep(time.toLong() * 1000)
            progressDialog.dismiss()
            tv_result.text = "Slept for $time seconds"
        }
    }
}

